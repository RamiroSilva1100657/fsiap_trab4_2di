
package fsiap20132014;

public class AnguloBrewster {
    
    float indicerefracao1;
    float indicerefracao2;
    float angulo_brewster;
    
    public AnguloBrewster(float n1, float n2){
        setIndice1(n1);
        setIndice2(n2);
    }
    
    public void setIndice1(float n1){
        indicerefracao1 = n1;
    }
    
    public void setIndice2(float n2){
        indicerefracao2 = n2;
    }
    
    public float getIndice1(){
        return indicerefracao1;
    }
    
    public float getIndice2(){
        return indicerefracao2;
    }
    
    public float calcularangulo(float n1, float n2){
        float angulo;
        angulo = (float) Math.atan(n2/n1);
        
        return (float) Math.toDegrees(angulo);
    }
    public void setAngulo(float n1, float n2){
        angulo_brewster = calcularangulo(n1,n2);
        
    }
    
    public float getAngulo(){
        return angulo_brewster;
    }
    
 

}
