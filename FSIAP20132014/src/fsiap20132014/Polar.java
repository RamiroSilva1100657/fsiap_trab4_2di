package fsiap20132014;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;

public class Polar extends JFrame {

    JButton bt1, bt2;
    JPanel p1;
    int contadorbt = 0;
    JTextPane textopola = new JTextPane();
    JLabel imagem = new JLabel();

    public Polar() {
        super("Polarização");
        setLayout(new BorderLayout());

        bt1 = new JButton("Processo");
        bt1.setPreferredSize(new Dimension(150, 50));
        bt1.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/amarelo.png")));
        bt1.setHorizontalTextPosition(JButton.CENTER);
        bt1.setVerticalTextPosition(JButton.CENTER);
        bt1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                polarprocesso();
            }
        });
        bt2 = new JButton("No dia-a-dia");
        bt2.setPreferredSize(new Dimension(150, 50));
        bt2.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/amarelo.png")));
        bt2.setHorizontalTextPosition(JButton.CENTER);
        bt2.setVerticalTextPosition(JButton.CENTER);
        bt2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                polardia();
            }
        });
        p1 = new JPanel();

        p1.add(bt1);
        p1.add(bt2);
        p1.setBackground(Color.black);

        add(p1, BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
    }

    JButton nxtb;
    JButton bckb;
    JPanel bpanel, butpanel;

    public void polarprocesso() {

        nxtb = new JButton();
        bckb = new JButton();

        final JFrame pola = new JFrame(" Processo de Polarização");
        pola.setResizable(false);
        bpanel = new JPanel(new BorderLayout());
        bpanel.setBackground(Color.black);
        pola.add(bpanel);
        textopola.setText(PolarTexto.texto1);
        imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/polar2.gif")));
        imagem.setVisible(true);

        textopola.setEnabled(false);
        textopola.setBackground(Color.black);
        textopola.setOpaque(true);
        bpanel.add(imagem, BorderLayout.CENTER);
        //bpanel.add(null,BorderLayout.WEST);
        //bpanel.add(null,BorderLayout.EAST);
        bpanel.add(textopola, BorderLayout.NORTH);

        nxtb.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/next.png")));
        nxtb.setBounds(600, 430, 35, 27);
        bpanel.setVisible(true);
        butpanel = new JPanel(new FlowLayout());
        butpanel.add(bckb);
        butpanel.add(nxtb);
        butpanel.setBackground(Color.BLACK);
        bpanel.add(butpanel, BorderLayout.SOUTH);

        nxtb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contadorbt++;
                if (contadorbt > 4) {
                    contadorbt = 4;
                }
                if (contadorbt == 0) {

                } else if (contadorbt == 1) {
                    textopola.setText(PolarTexto.texto2);
                    textopola.setText(PolarTexto.texto2);
                    imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/tipospola.jpg")));
                    imagem.setBounds(50, 170, 450, 300);
                    imagem.setVisible(true);
                    pola.setSize(new Dimension(700, 600));

                } else if (contadorbt == 2) {
                    textopola.setText(PolarTexto.texto3);
                    imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/texto3.png")));
                    imagem.setBounds(50, 170, 450, 300);
                    imagem.setVisible(true);
                    pola.setSize(new Dimension(700, 500));
                } else if (contadorbt == 3) {
                    textopola.setText(PolarTexto.texto4);
                    imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/texto4.png")));
                    imagem.setBounds(50, 170, 450, 300);
                    imagem.setVisible(true);
                    pola.setSize(new Dimension(700, 500));
                } else if (contadorbt == 4) {
                    textopola.setText(PolarTexto.texto5);
                    imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/polar5.png")));
                    imagem.setBounds(50, 170, 450, 300);
                    imagem.setVisible(true);
                    pola.setSize(new Dimension(900, 500));
                }

            }
        });

        bckb.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/back.png")));
        bckb.setBounds(550, 430, 35, 27);
        bckb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contadorbt--;
                if (contadorbt < 0) {
                    contadorbt = 0;
                }
                if (contadorbt == 0) {
                    textopola.setText(PolarTexto.texto1);
                    imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/polar2.gif")));
                    imagem.setBounds(50, 170, 450, 300);
                    imagem.setVisible(true);
                    nxtb.setVisible(true);
                   pola.setSize(new Dimension(700, 600));

                } else if (contadorbt == 1) {
                    textopola.setText(PolarTexto.texto2);
                    imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/tipospola.jpg")));
                    imagem.setBounds(50, 170, 450, 300);
                    imagem.setVisible(true);
                    pola.setSize(new Dimension(700, 600));
                } else if (contadorbt == 2) {
                    textopola.setText(PolarTexto.texto3);
                    imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/texto3.png")));
                    imagem.setBounds(50, 170, 450, 300);
                    imagem.setVisible(true);
                    pola.setSize(new Dimension(700, 500));
                } else if (contadorbt == 3) {
                    textopola.setText(PolarTexto.texto4);
                    imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/texto4.png")));
                    imagem.setBounds(50, 170, 450, 300);
                    imagem.setVisible(true);
                    pola.setSize(new Dimension(700, 500));
                } else if (contadorbt == 4) {
                    textopola.setText(PolarTexto.texto5);
                    imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/polar5.png")));
                    imagem.setBounds(50, 170, 450, 300);
                    imagem.setVisible(true);
                    pola.setSize(new Dimension(900, 500));
                }
            }
        });
        pola.setResizable(false);
        pola.setSize(new Dimension(700, 600));
        pola.setLocationRelativeTo(null);
        pola.setAlwaysOnTop(true);
        pola.setVisible(true);

    }

    public void polardia() {

        final JFrame pola = new JFrame(" Processo de Polarização");
        pola.setResizable(false);
        bpanel = new JPanel(new BorderLayout());
        bpanel.setBackground(Color.black);
        pola.add(bpanel);
        textopola.setText(PolarTexto.texto6);
        imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/lcd.jpg")));
        imagem.setVisible(true);

        textopola.setEnabled(false);
        textopola.setBackground(Color.black);
        textopola.setOpaque(true);
        bpanel.add(imagem, BorderLayout.CENTER);
        bpanel.add(textopola, BorderLayout.NORTH);
        bpanel.setVisible(true);
      
        pola.setResizable(false);
        pola.setSize(new Dimension(818, 600));
        pola.setLocationRelativeTo(null);
        pola.setAlwaysOnTop(true);
        pola.setVisible(true);

    }
}
