/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap20132014;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;

/**
 *
 * @author 0
 */
public class LeiMalusUI extends JFrame {

    JButton bt1, bt2, calc;
    JPanel p1;
    JPanel bpanel, butpanel, labelpanel;
    Object[][] data;
    Object[] column;
    JTable fixedTable, table;
    JTextPane txt;

    public LeiMalusUI() {
        super("Lei de Malus");
        setLayout(new BorderLayout());

        bt1 = new JButton("Lei de Malus Enunciada");
        bt1.setPreferredSize(new Dimension(200, 50));
        bt1.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/amarelo.png")));
        bt1.setHorizontalTextPosition(JButton.CENTER);
        bt1.setVerticalTextPosition(JButton.CENTER);
        bt1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                leimalusenunciada();
            }
        });
        bt2 = new JButton("Experiência");
        bt2.setPreferredSize(new Dimension(200, 50));
        bt2.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/amarelo.png")));
        bt2.setHorizontalTextPosition(JButton.CENTER);
        bt2.setVerticalTextPosition(JButton.CENTER);
        bt2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                experiência();
            }
        });
        p1 = new JPanel();

        p1.add(bt1);
        p1.add(bt2);
        p1.setBackground(Color.black);

        add(p1, BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
    }

    public void leimalusenunciada() {

        JFrame malus = new JFrame("Lei de Malus");
        malus.setResizable(false);

        JPanel pp = new JPanel(new BorderLayout(700, 500));
        pp.setBackground(Color.black);

        malus.add(pp);

        JLabel ubreakingmaluslaw = new JLabel();
        ubreakingmaluslaw.setSize(new Dimension(320, 250));

        txt = new JTextPane();
        txt.setText("Quando um segundo polarizador é rodado, o componente de "
                + "vector perpendicular ao seu plano de transmissão é absorvido, "
                + "reduzindo a sua amplitude.\n"
                + "Uma vez que a intensidade transmitida é proporcional ao "
                + "quadrado da amplitude, a intensidade é dada por:\n"
                + "Is = Ie.cos²θ\nIs: Intensidade de saída\nIe: Intensidade de entrada");
        txt.setEnabled(false);
        txt.setBackground(Color.black);
        txt.setOpaque(true);
        JPanel p = new JPanel( new BorderLayout());
        p.setVisible(true);
        p.setBackground(Color.black);
        p.setBounds(350,50,350,250);
        p.add(txt, BorderLayout.CENTER);
        pp.add(p);
        JLabel int0 = new JLabel("Intensidade Inicial");
        int0.setBounds(320, 380, 120, 30);
        JLabel angi = new JLabel("Angulo");
        angi.setBounds(320, 380, 160, 80);
        JLabel fin = new JLabel("Intensidade:");
        fin.setBounds(320, 380, 200, 130);
        pp.add(fin);
        pp.add(angi);
        final JTextField itext = new JTextField(20);
        itext.setBounds(430, 385, 50, 20);
        final JTextField itextt = new JTextField(20);
        itextt.setBounds(370, 410, 50, 20);
        final JLabel resl = new JLabel("");
        resl.setBounds(390, 435, 111, 20);

        calc = new JButton("Calcular");
        calc.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/amarelo.png")));
        calc.setBounds(600, 420, 300, 50);
        calc.setHorizontalTextPosition(JButton.CENTER);
        calc.setVerticalTextPosition(JButton.CENTER);
        calc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                float calculosi;
                float iii = Float.parseFloat(itext.getText());
                double ii3 = Double.parseDouble(itextt.getText());
                calculosi = (float) ((iii / 2) * (Math.pow(Math.cos(Math.abs(ii3)), 2)));
                String s = " " + calculosi + "W/m^2";
                resl.setText(s);

            }
        });
        pp.add(calc);
        pp.add(resl);
        pp.add(itext);
        pp.add(itextt);
        pp.add(int0);
        ubreakingmaluslaw.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/polmalus.png")));
        pp.add(ubreakingmaluslaw);
        malus.setSize(new Dimension(800, 500));
        malus.setLocationRelativeTo(null);
        malus.setAlwaysOnTop(true);
        malus.setVisible(true);
    }

    public void experiência() {

        final JFrame maluUI = new JFrame("Experiência - Lei de Malus");
        maluUI.setResizable(false);
        bpanel = new JPanel(new BorderLayout());
        bpanel.setBackground(Color.white);
        maluUI.add(bpanel);

        final JLabel i0 = new JLabel("Intensidade inicial");
        final JLabel a1 = new JLabel("Angulo 1:");
        final JLabel a2 = new JLabel("Angulo 2:");
        final JLabel a3 = new JLabel("Angulo 3:");
        final JLabel a4 = new JLabel("Angulo 4:");
        final JTextField ti0 = new JTextField(2);
        final JTextField ta1 = new JTextField(2);
        final JTextField ta2 = new JTextField(2);
        final JTextField ta3 = new JTextField(2);
        final JTextField ta4 = new JTextField(2);

        labelpanel = new JPanel(new FlowLayout());
        labelpanel.add(i0);
        labelpanel.add(ti0);
        labelpanel.add(a1);
        labelpanel.add(ta1);
        labelpanel.add(a2);
        labelpanel.add(ta2);
        labelpanel.add(a3);
        labelpanel.add(ta3);
        labelpanel.add(a4);
        labelpanel.add(ta4);

        bpanel.add(labelpanel, BorderLayout.NORTH);

        final JTextPane resultados = new JTextPane();
        resultados.setText("Intensidade 1: \nIntensidade 2: \nIntensidade 3: \nIntensidade 4:");
        //resultados.setBounds(100, 100, 50, 50);
        resultados.setPreferredSize(new Dimension(10, 10));
        resultados.setEnabled(false);
        resultados.setBackground(new Color(139, 139, 131));

        bpanel.add(resultados, BorderLayout.CENTER);
        //bpanel.add(null,BorderLayout.EAST);
        //bpanel.add(textopola, BorderLayout.NORTH);

        JButton calcular = new JButton("Calcular");
        JButton verdados = new JButton("Ver Dados");

        butpanel = new JPanel(new FlowLayout());
        butpanel.add(calcular);
        butpanel.add(verdados);
        //butpanel.setBackground(Color.BLACK);
        bpanel.add(butpanel, BorderLayout.SOUTH);

        calcular.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    float si0 = Float.parseFloat(ti0.getText());
                    double sa1 = Double.parseDouble(ta1.getText());
                    double sa2 = Double.parseDouble(ta2.getText());
                    double sa3 = Double.parseDouble(ta3.getText());
                    double sa4 = Double.parseDouble(ta4.getText());

                    LeiMalus a = new LeiMalus(si0, sa1, sa2, sa3, sa4);
                    a.setIntensidadeI1();
                    a.setIntensidadeI2();
                    a.setIntensidadeI3();
                    a.setIntensidadeI4();
                    System.out.println(a.getAngulo1());

                    resultados.setText("Intensidade 1: " + a.intensidade1 + " W/(m^2)" + " \nIntensidade 2: " + a.intensidade2 + " W/(m^2)" + " \nIntensidade 3: " + a.intensidade3 + " W/(m^2)" + " \nIntensidade 4: " + a.intensidade4 + " W/(m^2)");

                    MalusExperiment b = new MalusExperiment();
                    b.adicionarIntensidade1(a.getIntensidadeI1(), a.angulo1);
                    b.adicionarIntensidade2(a.getIntensidadeI2(), a.angulo2);
                    b.adicionarIntensidade3(a.getIntensidadeI3(), a.angulo3);
                    b.adicionarIntensidade4(a.getIntensidadeI4(), a.angulo4);
                } catch (NumberFormatException r) {

                    resultados.setText("ERRO. VERIFIQUE SE OS DADOS INTRODUZIDOS ENCONTRAM-SE CORRECTOS!");

                }
            }

        });

        verdados.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LeiMalusTable table = new LeiMalusTable();

                table.setVisible(true);
            }

        }
        );

        maluUI.setResizable(
                false);
        maluUI.setSize(
                new Dimension(750, 400));
        maluUI.setLocationRelativeTo(
                null);
        maluUI.setAlwaysOnTop(
                true);
        maluUI.setVisible(
                true);

    }

}
