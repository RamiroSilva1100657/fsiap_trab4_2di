/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fsiap20132014;

import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author 0
 */
public class LeiMalusTable extends JFrame {
  Object[][] data;
  Object[] column;
  JTable fixedTable,table; 
  public ArrayList<Float> array0 = new ArrayList();
    public ArrayList<LeiMalus> array1 = new ArrayList();
    public ArrayList<LeiMalus> array2 = new ArrayList();
    public ArrayList<LeiMalus> array3 = new ArrayList();
    public ArrayList<LeiMalus> array4 = new ArrayList();
  

 
  public LeiMalusTable() {
    super( "Dados Lei de Malus" );
    setSize( 695, 250 );
    
    /*data =  new Object[][]{
        {"1","11","A","","","","","","",""},
        {"2","22","","B","","","","","",""},
        {"3","33","","","C","","","","",""},
        {"4","44","","","","D","","","",""},
        {"5","55","","","","","E","","",""},
        {"6","66","","","","","","F","",""}};*/
    MalusExperiment abc = new MalusExperiment();
    array0 = abc.arrayi0;
    array1 = abc.arrayi1;
    array2 = abc.arrayi2;
    array3 = abc.arrayi3;
    array4 = abc.arrayi4;
    
    column = new Object[]{"I0","I1","A1","I2","A2","I3","A3","I4","A4"};
        
    AbstractTableModel fixedModel = new AbstractTableModel() {
      public int getColumnCount() { return 2; }
      public int getRowCount() { return data.length; }
      public String getColumnName(int col) {
       return (String)column[col]; 
      }
      public Object getValueAt(int row, int col) { 
        return data[row][col]; 
      }
    };
    AbstractTableModel    model = new AbstractTableModel() {
      public int getColumnCount() { return column.length -2; }
      public int getRowCount() { return data.length; }
      public String getColumnName(int col) {
       return (String)column[col +2]; 
      }
      public Object getValueAt(int row, int col) { 
        return data[row][col+2]; 
      }
      public void setValueAt(Object obj, int row, int col) { 
        data[row][col +2] = obj; 
      }
      public boolean CellEditable(int row, int col) { 
        return true; 
      }
    };
    
    fixedTable = new JTable( fixedModel ) {
      public void valueChanged(ListSelectionEvent e) {
        super.valueChanged(e);
        checkSelection(true); 
      }
    };
    table = new JTable( model ) {
      public void valueChanged(ListSelectionEvent e) {
        super.valueChanged(e);
        checkSelection(false); 
      }
    };
    fixedTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    fixedTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    
    JScrollPane scroll = new JScrollPane( table );
    JViewport viewport = new JViewport();
    viewport.setView(fixedTable);
    viewport.setPreferredSize(fixedTable.getPreferredSize());
    scroll.setRowHeaderView(viewport);
    scroll.setCorner(JScrollPane.UPPER_LEFT_CORNER,fixedTable.getTableHeader());
    
    getContentPane().add(scroll, BorderLayout.CENTER);
  }
  
  private void checkSelection(boolean isFixedTable) {
    int fixedSelectedIndex = fixedTable.getSelectedRow();
    int      selectedIndex = table.getSelectedRow();
    if (fixedSelectedIndex != selectedIndex) {
      if (isFixedTable) {
        table.setRowSelectionInterval(fixedSelectedIndex,fixedSelectedIndex);
      } else {
        fixedTable.setRowSelectionInterval(selectedIndex,selectedIndex);
      }
    }
  }

 
}