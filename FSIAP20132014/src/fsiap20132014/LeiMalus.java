package fsiap20132014;

public class LeiMalus {

    float intensidade_inicial;
    double angulo;
    float intensidade1;
    double angulo1;
    float intensidade2;
    double angulo2;
    float intensidade3;
    double angulo3;
    float intensidade4;
    double angulo4;

    /**
     * I= Io/2 * cosAngulo 2 Lei de Malus-> Sendo uma onda idealmente polarizada
     * 100 por cento por um polarizador a sua intensidade é dada por 1/2 I0(
     * Intensidade Inicial);
     */
    public LeiMalus(float i0, double a1, double a2, double a3, double a4) {
        setIntensidadeInicial(i0);
        setAngulo1(a1);
        setAngulo2(a2);
        setAngulo3(a3);
        setAngulo4(a4);

    }

    public LeiMalus(float i0, double a1) {
        setIntensidadeInicial(i0);
        setAngulo1(a1);

    }

    public void setIntensidadeInicial(float i0) {
        intensidade_inicial = i0;
    }

    public void setAngulo1(double a1) {

        angulo1 = a1;
    }

    public void setAngulo2(double a2) {
        angulo2 = a2;
    }

    public void setAngulo3(double a3) {
        angulo3 = a3;
    }

    public void setAngulo4(double a4) {
        angulo4 = a4;
    }

    public double getAngulo1() {
        return angulo1;
    }

    public double getAngulo2() {
        return angulo2;
    }

    public double getAngulo3() {
        return angulo3;
    }

    public double getAngulo4() {
        return angulo4;
    }

    public float calcularIntensidadeI1(float i, double a) {

        float calculo;
        if (a == 90 || a == 270) {
            calculo = 0;

        } else {
            calculo = (float) ((i / 2) * (Math.pow(Math.cos(a), 2)));
        }
        return calculo;
    }

    public float calcularIntensidadeI2(float i, double a) {
        float calculo;
        a = getAngulo2();
        double at = getAngulo1();
        if (a == 90 || a == 270) {
            calculo = 0;

        } else if (at == 0 && a == 90) {
            calculo = 0;
        } else if (at == 90 && a == 0) {
            calculo = 0;
        } else if (at == 180 && a == 90) {
            calculo = 0;
        } else {
            calculo = (float) (i * (Math.pow(Math.cos(Math.abs(a - at)), 2)));
        }

        return calculo;
    }

    public float calcularIntensidadeI3(float i, double a) {
        float calculo;
        a = getAngulo3();
        double at = getAngulo2();
        if (a == 90 || a == 270) {
            calculo = 0;

        } else if (at == 0 && a == 90) {
            calculo = 0;
        } else if (at == 90 && a == 0) {
            calculo = 0;
        } else if (at == 180 && a == 90) {
            calculo = 0;
        } else {
            calculo = (float) (i * (Math.pow(Math.cos(Math.abs(a - at)), 2)));
        }

        return calculo;
    }

    public float calcularIntensidadeI4(float i, double a) {
        float calculo;
        a = getAngulo4();
        double at = getAngulo3();
        if (a == 90 || a == 270) {
            calculo = 0;

        } else if (at == 0 && a == 90) {
            calculo = 0;
        } else if (at == 90 && a == 0) {
            calculo = 0;
        } else if (at == 180 && a == 90) {
            calculo = 0;
        } else {
            calculo = (float) (i * (Math.pow(Math.cos(Math.abs(a - at)), 2)));
        }

        return calculo;
    }

    public void setIntensidadeI1() {
        intensidade1 = calcularIntensidadeI1(intensidade_inicial, angulo1);
    }

    public void setIntensidadeI2() {
        intensidade2 = calcularIntensidadeI2(intensidade1, angulo2);
    }

    public void setIntensidadeI3() {

        intensidade3 = calcularIntensidadeI3(intensidade2, angulo3);
    }

    public void setIntensidadeI4() {
        intensidade4 = calcularIntensidadeI4(intensidade3, angulo4);
    }

    public float getIntensidadeI1() {
        return intensidade1;
    }

    public float getIntensidadeI2() {
        return intensidade2;
    }

    public float getIntensidadeI3() {
        return intensidade3;
    }

    public float getIntensidadeI4() {
        return intensidade4;
    }

    public double getAngulo() {
        return angulo;
    }

    public float getIntensidadeInicial() {
        return intensidade_inicial;
    }

    

}
