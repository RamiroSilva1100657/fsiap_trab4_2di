package fsiap20132014;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class PolarTexto {

    static JLabel imagem1 = new JLabel(new ImageIcon("./misc/polar2.gif"));

    static String texto1 = "Por definição, a polarização de uma onda electromagnética é o plano no qual se encontra a componente ELÉCTRICA desta onda."
            + "Toda a onda electromagnética é composta por dois campos, o eléctrico e o magnético, sempre situados em planos"
            + " ortogonais (planos fisicamente a 90 graus), e variando em fase (0 graus). Estes campos se propagam em qualquer material isolante "
            + "(dieléctrico) com uma velocidade de propagação, cujo vector está a 90 graus dos vetores campo elétrico e magnético. No vácuo, esta velocidade"
            + " é a da luz. Um dipolo posicionado verticalmente, alimentado por um gerador de  frequência F, gera portanto uma onda eletromagnética polarizada"
            + " verticalmente, pois o componente campo eléctrico está no plano vertical (e consequentemente, a componente do campo magnético está no plano horizontal)."
            + " \nNa figura seguinte, aparecem os três vectores E, B e V, com 90 graus físicos entre qualquer um deles, com E e B variando em fase ou com "
            + "zero graus de defasamento eléctrico, característica básica da onda eletromagnética e como esta onda está sempre situada no mesmo plano, é chamada de onda com polarização linear:";

    static String texto2 = "Existem três tipos de polarização:\n\n"
            + "1. Polarização linear: o campo eléctrico permanece sempre no mesmo plano.\n"
            + "\n2. Polarização circular: o campo eléctrico e magnético permanecem constantes em"
            + " magnitude, mas giram ao redor da direcção de propagação. \n\n"
            + "             E = j.E0.sen(kx−wt) + k.E0.cos(kx−wt)\n"
            + "\n3. Polarização elíptica: a amplitude das componentes ortogonais do campo eléctrico"
            + " são diferentes.\n\n"
            + "             E = j E0.sen(kx−wt) + k.2.E0.cos(kx−wt)";

    static String texto3 = "A maioria das OEM produzidas por uma única fonte são polarizadas. Entretanto, nas "
            + "fontes comuns de luz (Sol, lâmpada fluorescente), os radiadores, que são os átomos "
            + "constituintes da fonte, atuam independentemente uns dos outros. Como "
            + "consequência, a luz emitida consiste de várias ondas independentes cujos planos de "
            + "vibração se acham orientados aleatoriamente. Dizemos que essas ondas são não polarizadas"
            + "\n\nTeoricamente, podemos decompor cada campo elétrico da onda não polarizada em "
            + "componentes perpendiculares enter si.";

    static String texto4 = "Podemos transformar luz originalmente não-polarizada em luz polarizada fazendo-a "
            + "passar por uma placa polarizadora. No plano da placa existe uma direção "
            + "característica chamada direção de polarização. Apenas os componentes dos "
            + "vetores paralelos à direção de polarização são transmitidos. Os componentes "
            + "perpendiculares são absorvidos."
            + "\n\nPolarizador ideal: transmite 100% da luz na direção "
            + "de polarização e bloqueia totalmente a luz na "
            + "direção perpendicular."
            + "\n\nPolarizador real: aproximadamente 80% de "
            + "transmissão e 99% de bloqueio.";

    static String texto5 = "Quando se faz passar luz não-polarizada através de um polarizador, a intensidade transmitida é metade da intensidade original.";
           
    static String texto6 = "A polarização é muito usada nos dias de hoje sem que nos apercebamos. Pode ser usada nos óculos de sol,"
            + " pode ser usada também nos televisores LCD, rádios e entre muitas outras aplicações."
            + "\nTodas as antenas de recepção e transmissão a rádio são intrinsecamente polarizadas, uso especial do qual se faz em "
            + "radares. A maioria de antenas irradia polarizações horizontal, vertical ou circular, embora também haja polarização elíptica. "
            + "O campo elétrico ou o plano E determinam a polarização ou orientação da onda magnética. A polarização vertical é mais frequentemente "
            + "usada quando se deseja transmitir um sinal de rádio em todas as direções, tais como unidades móveis amplamente distribuídas.\n"
            + "Os filtros polarizadores são usados também na fotografia. Podem aprofundar a cor de um céu azul e eliminar reflexões de janelas e da água diretamente."
            + " Os televisores LCD usam cristais líquidos para controlar a passagem de luz em cada pixel. Estes cristais liquidos são usados também em relógios, ecrãs "
            + "de computador... Nos óculos solares são também usados materias polarizadoras o que facílita muito, por exemplo, na condução (a luz solar é reflectida na estrada"
            + "o que pode fazer com que a pessoa não consiga ver nada).";
    
}
