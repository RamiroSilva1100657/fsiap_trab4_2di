package fsiap20132014;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;

public class Electro extends JFrame {

    JButton bt1, bt2;
    JPanel p1;
    int contadorbt = 0;
    JTextPane textoelec = new JTextPane();
    JLabel imagem = new JLabel();

    String txtrad = "A radiação eletromagnética é constituída por ondas que se auto-propagam pelo espaço."
            + "Parte de todo o espectro consegue ser interpretada através do olho dos diversos animais e, "
            + "para cada espécie, denomina-se essa faixa de radiação de luz visível. A radiação eletromagnética "
            + "compõe-se de um campo elétrico e um magnético, que oscilam perpendicularmente um ao outro e à direção "
            + "da propagação de energia. A radiação eletromagnética é classificada de acordo com a frequência da onda, "
            + "que em ordem decrescente da duração (período T) da onda são: ondas de rádio, micro-ondas, radiação terahertz "
            + "(Raios T), radiação infravermelha, luz visível, radiação ultravioleta, Raios-X e Radiação Gama.";

    String txtonda = "As ondas eletromagnéticas primeiramente foram previstas teoricamente por "
            + "James Clerk Maxwell e depois confirmadas experimentalmente por Heinrich Hertz. "
            + "Maxwell notou as ondas a partir de equações de electricidade e magnetismo, revelando "
            + "sua natureza e sua simetria. Faraday mostrou que um campo magnético variável no tempo gera "
            + "um campo eléctrico. Maxwell mostrou que um campo eléctrico variável com o tempo gera um campo magnético, "
            + "com isso há uma auto-sustentação entre os campos eléctrico e magnético. Em seu trabalho de 1862, Maxwell escreveu:"
            + "\n'A velocidade das ondas transversais em nosso meio hipotético, calculada a partir dos experimentos "
            + "electromagnéticos dos Srs. Kohrausch e Weber, concorda tão exactamente com a velocidade da luz, "
            + "calculada pelos experimentos óticos do Sr. Fizeau, que é difícil evitar a inferência de que a luz "
            + "consiste nas ondulações transversais do mesmo meio que é a causa dos fenómenos eléctricos e magnéticos.'";

    String txtexpetro = "Espectro Eletromagnético é classificado normalmente pelo comprimento da onda, "
            + "como as ondas de rádio, as micro-ondas, a radiação infravermelha, a luz visível, os raios "
            + "ultravioleta, os raios X, até a radiação gama.\n"
            + "O comportamento da onda eletromagnética depende do seu comprimento de onda. "
            + "Frequências altas são curtas, e frequências baixas são longas. Quando uma onda interage "
            + "com uma única partícula ou molécula, seu comportamento depende da quantidade de fótons por "
            + "ela carregada. Através da técnica denominada Espectroscopia óptica, é possível "
            + "obter-se informações sobre uma faixa visível mais larga do que a visão normal. Um espectroscópio "
            + "comum pode detectar comprimentos de onda de 2 nm a 2500 nm.\n"
            + "Essas informações detalhadas podem informar propriedades físicas dos objetos, gases e "
            + "até mesmo estrelas. Por exemplo, um átomo de hidrogênio emite ondas em comprimentos de 21,12 cm."
            + " A luz propriamente dita corresponde à faixa que é detectada pelo olho humano, entre 400 nm a 700 nm "
            + "(um nanômetro vale 1,0×10-9 metro). As ondas de rádio são formadas de uma combinação de amplitude, frequência "
            + "e fase da onda com a banda da frequência.";

    public Electro() {
        super("Radiação e Ondas Eletromagnética");
        setLayout(new BorderLayout());

        bt1 = new JButton("Teoria");
        bt1.setPreferredSize(new Dimension(150, 50));
        bt1.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/amarelo.png")));
        bt1.setHorizontalTextPosition(JButton.CENTER);
        bt1.setVerticalTextPosition(JButton.CENTER);
        bt1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rad();
            }
        });
        bt2 = new JButton("No dia-a-dia");
        bt2.setPreferredSize(new Dimension(150, 50));
        bt2.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/amarelo.png")));
        bt2.setHorizontalTextPosition(JButton.CENTER);
        bt2.setVerticalTextPosition(JButton.CENTER);
        bt2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dia();
            }
        });
        p1 = new JPanel();

        p1.add(bt1);
        p1.add(bt2);
        p1.setBackground(Color.black);

        add(p1, BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
    }

    JButton nxtb;
    JButton bckb;
    JPanel bpanel, butpanel;

    public void rad() {
        nxtb = new JButton();
        bckb = new JButton();

        final JFrame elec = new JFrame("Radiação Eletromagnética");

        bpanel = new JPanel(new BorderLayout());
        bpanel.setBackground(Color.black);
        elec.add(bpanel);

        textoelec.setText(txtrad);

        imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/polarElec.gif")));
        imagem.setVisible(true);

        textoelec.setEnabled(false);
        textoelec.setBackground(Color.black);
        textoelec.setOpaque(true);

        bpanel.add(imagem, BorderLayout.CENTER);
        //bpanel.add(null,BorderLayout.WEST);
        //bpanel.add(null,BorderLayout.EAST);
        bpanel.add(textoelec, BorderLayout.NORTH);

        nxtb.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/next.png")));
        nxtb.setBounds(600, 430, 35, 27);
        bpanel.setVisible(true);
        butpanel = new JPanel(new FlowLayout());
        butpanel.add(bckb);
        butpanel.add(nxtb);
        butpanel.setBackground(Color.BLACK);
        bpanel.add(butpanel, BorderLayout.SOUTH);

        nxtb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contadorbt++;
                if (contadorbt > 2) {
                    contadorbt = 2;
                }
                if (contadorbt == 0) {

                } else if (contadorbt == 1) {
                    elec.setTitle("Ondas Eletromagnéticas");
                    textoelec.setText(txtonda);
                    imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/polarOnda.png")));
                    imagem.setBounds(50, 170, 450, 300);
                    imagem.setVisible(true);
                } else {
                    elec.setTitle("Espetro Eletromagnético");
                    textoelec.setText(txtexpetro);
                    imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/polaEsp.png")));
                    imagem.setBounds(50, 170, 450, 300);
                    imagem.setVisible(true);
                }
            }
        });

        bckb.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/back.png")));
        bckb.setBounds(550, 430, 35, 27);
        bckb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contadorbt--;
                if (contadorbt < 0) {
                    contadorbt = 0;
                }
                if (contadorbt == 0) {
                    elec.setTitle("Radiação Eletromagnética");
                    textoelec.setText(txtrad);
                    imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/polarElec.gif")));
                    imagem.setBounds(50, 170, 450, 300);
                    imagem.setVisible(true);
                    nxtb.setVisible(true);

                } else if (contadorbt == 1) {
                    elec.setTitle("Ondas Eletromagnéticas");
                    textoelec.setText(txtonda);
                    imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/polarOnda.png")));
                    imagem.setBounds(50, 170, 450, 300);
                    imagem.setVisible(true);
                } else {
                    elec.setTitle("Espetro Eletromagnético");
                    textoelec.setText(txtexpetro);
                    imagem.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/polaEsp.png")));
                    imagem.setBounds(50, 170, 450, 300);
                    imagem.setVisible(true);
                }
            }
        });
        elec.setResizable(false);
        elec.setSize(new Dimension(700, 600));
        elec.setLocationRelativeTo(null);
        elec.setAlwaysOnTop(true);
        elec.setVisible(true);

    }

    public void dia() {

        JFrame diadia = new JFrame("Dia-a-Dia");
        bpanel = new JPanel(new BorderLayout());
        bpanel.setBackground(Color.black);

        JPanel p1 = new JPanel();
        p1.setBackground(Color.black);
        JTextPane txt = new JTextPane();

        txt.setText("Entre inúmeras aplicações destacam-se o rádio, a televisão, "
                + "radares, os sistemas de comunicação sem fio (telefonia celular "
                + "e comunicação wi-fi), os sistemas de comunicação baseados em fibras "
                + "ópticas e fornos de micro-ondas. Existem equipamentos para a esterilização "
                + "de lâminas baseados na exposição do instrumento a determinada radiação ultravioleta, "
                + "produzida artificialmente por uma lâmpada de luz negra");
        txt.setEnabled(false);
        txt.setBackground(Color.black);
        txt.setOpaque(true);
        bpanel.add(txt, BorderLayout.NORTH);
        p1.add(new JLabel(new ImageIcon(FSIAP20132014.class.getResource("/Misc/ele.jpg"))));

        bpanel.add(p1, BorderLayout.CENTER);
        diadia.add(bpanel);
        diadia.setResizable(false);
        diadia.setSize(new Dimension(700, 500));
        diadia.setLocationRelativeTo(null);
        diadia.setAlwaysOnTop(true);
        diadia.setVisible(true);

    }
}
