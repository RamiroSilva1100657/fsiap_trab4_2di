package fsiap20132014;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;

public class FSIAP20132014 {

    public static void main(String[] args) {
        new Janela();
    }
}

class Janela extends JFrame {

    JButton bt1, bt2, bt3, bt4;
    JLabel label[];

    public Janela() {
        super("Polarização das ondas electromagnéticas");
        setLayout(new BorderLayout(20, 20));

        JPanel p1 = new JPanel(new FlowLayout());

        bt1 = new JButton("Radiação e Ondas Eletromagnética");
        bt1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new Electro();
            }
        });
        bt1.setPreferredSize(new Dimension(250, 100));
        bt1.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/amarelo.png")));
        bt1.setHorizontalTextPosition(JButton.CENTER);
        bt1.setVerticalTextPosition(JButton.CENTER);
        bt2 = new JButton("Polarização");
        bt2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Polar p = new Polar();
            }

        });
        bt2.setPreferredSize(new Dimension(250, 100));
        bt2.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/amarelo.png")));
        bt2.setHorizontalTextPosition(JButton.CENTER);
        bt2.setVerticalTextPosition(JButton.CENTER);
        bt3 = new JButton("Lei de Malus");
        bt3.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LeiMalusUI l = new LeiMalusUI();
            }
        });
        bt3.setPreferredSize(new Dimension(250, 100));
        bt3.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/amarelo.png")));
        bt3.setHorizontalTextPosition(JButton.CENTER);
        bt3.setVerticalTextPosition(JButton.CENTER);
        bt4 = new JButton("Ângulo de Brewster");
        bt4.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame abrewster = new JFrame("Ângulo de Brewster");
                abrewster.setResizable(false);
                JPanel bpanel = new JPanel(new BorderLayout(1000, 700));
                bpanel.setBackground(Color.white);

                abrewster.add(bpanel);
                JLabel imgbrewster = new JLabel();
                imgbrewster.setSize(new Dimension(220, 150));
                imgbrewster.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/brewster.gif")));

                JLabel i0 = new JLabel("Índice refração 1 (n1):");
                i0.setBounds(520, 70, 150, 30);
                final JTextField i10 = new JTextField(20);
                i10.setBounds(650, 70, 50, 30);
                bpanel.add(i10);
                bpanel.add(i0);

                JLabel a0 = new JLabel("Índice refração 2 (n2):");
                a0.setBounds(520, 100, 150, 30);
                final JTextField a10 = new JTextField(20);
                a10.setBounds(650, 100, 50, 30);
                bpanel.add(a10);
                bpanel.add(a0);

                JButton calc = new JButton("Calcular ângulo");
                calc.setBounds(750, 100, 150, 40);
                calc.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {

                        float intinicial = Float.parseFloat(i10.getText());
                        float angulo = Float.parseFloat(a10.getText());

                        AnguloBrewster ab = new AnguloBrewster(intinicial, angulo);
                        JOptionPane.showMessageDialog(rootPane, "Ângulo de Brewster:" + ab.calcularangulo(intinicial, angulo) + "º");
                        
                    }
                });
                bpanel.add(calc);
                bpanel.add(imgbrewster);
                abrewster.setSize(new Dimension(1000, 700));
                abrewster.setLocationRelativeTo(null);
                abrewster.setVisible(true);

            }
        });
        bt4.setPreferredSize(new Dimension(250, 100));
        bt4.setIcon(new ImageIcon(FSIAP20132014.class.getResource("/Misc/amarelo.png")));
        bt4.setHorizontalTextPosition(JButton.CENTER);
        bt4.setVerticalTextPosition(JButton.CENTER);

        p1.add(bt1);
        p1.add(bt2);
        p1.add(bt3);
        p1.add(bt4);
        p1.setBackground(Color.black);
        add(p1, BorderLayout.CENTER);

        setResizable(false);
        setVisible(true);
        pack();

        this.setLocationRelativeTo(null);

        addWindowListener(
                new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        Object[] opSimNao = {"Sim", "Não"};
                        if (JOptionPane.showOptionDialog(Janela.this, "Tem a certeza que pretende sair?", "Sair", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, opSimNao, opSimNao[1]) == JOptionPane.YES_OPTION) {
                            setDefaultCloseOperation(EXIT_ON_CLOSE);
                        } else {
                            setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
                        }
                    }
                });
    }
}
